<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyJournalRequest;
use App\Http\Requests\StoreJournalRequest;
use App\Http\Requests\UpdateJournalRequest;
use App\Models\Journal;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JournalController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('journal_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $journals = Journal::all();

        return view('admin.journals.index', compact('journals'));
    }

    public function create()
    {
        abort_if(Gate::denies('journal_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.journals.create');
    }

    public function store(StoreJournalRequest $request)
    {
        $journal = Journal::create($request->all());

        return redirect()->route('admin.journals.index');
    }

    public function edit(Journal $journal)
    {
        abort_if(Gate::denies('journal_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.journals.edit', compact('journal'));
    }

    public function update(UpdateJournalRequest $request, Journal $journal)
    {
        $journal->update($request->all());

        return redirect()->route('admin.journals.index');
    }

    public function show(Journal $journal)
    {
        abort_if(Gate::denies('journal_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.journals.show', compact('journal'));
    }

    public function destroy(Journal $journal)
    {
        abort_if(Gate::denies('journal_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $journal->delete();

        return back();
    }

    public function massDestroy(MassDestroyJournalRequest $request)
    {
        Journal::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
