<?php

namespace App\Http\Requests;

use App\Models\Article;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreArticleRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('article_create');
    }

    public function rules()
    {
        return [
            'title' => [
                'string',
                'min:3',
                'nullable',
            ],
            'description' => [
                'required',
            ],
            'themes.*' => [
                'integer',
            ],
            'themes' => [
                'array',
            ],
            'user_id' => [
                'required',
                'integer',
            ],
            'article_date' => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
        ];
    }
}
