<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## Progetto realizzato in classe per COMA SGT2A 2021-22

si basa sul Trello (dashboard Kanbahn) https://trello.com/b/mWs0ZWRG/kanban-sgt2a

Il codice si basa quasi escusivamente sul generatore Quickadminpanel (https://quickadminpanel.com)


### Partecipanti

- **Classe SGT2A 2021-22**
- **docente Roberto Otupacca**


## Licenza

Il codice è open-source e si basa sulla licenza MIT [MIT license](https://opensource.org/licenses/MIT).
