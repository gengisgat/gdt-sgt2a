<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToArticlesTable extends Migration
{
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_fk_5593979')->references('id')->on('users');
            $table->unsignedBigInteger('journal_id')->nullable();
            $table->foreign('journal_id', 'journal_fk_5593986')->references('id')->on('journals');
        });
    }
}
