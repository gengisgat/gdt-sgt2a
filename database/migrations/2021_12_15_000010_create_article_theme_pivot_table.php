<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleThemePivotTable extends Migration
{
    public function up()
    {
        Schema::create('article_theme', function (Blueprint $table) {
            $table->unsignedBigInteger('article_id');
            $table->foreign('article_id', 'article_id_fk_5593920')->references('id')->on('articles')->onDelete('cascade');
            $table->unsignedBigInteger('theme_id');
            $table->foreign('theme_id', 'theme_id_fk_5593920')->references('id')->on('themes')->onDelete('cascade');
        });
    }
}
