@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.article.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.articles.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">{{ trans('cruds.article.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}">
                @if($errors->has('title'))
                    <div class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.article.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.article.fields.description') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description') !!}</textarea>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.article.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="themes">{{ trans('cruds.article.fields.theme') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('themes') ? 'is-invalid' : '' }}" name="themes[]" id="themes" multiple>
                    @foreach($themes as $id => $theme)
                        <option value="{{ $id }}" {{ in_array($id, old('themes', [])) ? 'selected' : '' }}>{{ $theme }}</option>
                    @endforeach
                </select>
                @if($errors->has('themes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('themes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.article.fields.theme_helper') }}</span>
            </div>
{{--            <div class="form-group">--}}
{{--                <label class="required" for="user_id">{{ trans('cruds.article.fields.user') }}</label>--}}
{{--                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>--}}
{{--                    @foreach($users as $id => $entry)--}}
{{--                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--                @if($errors->has('user'))--}}
{{--                    <div class="invalid-feedback">--}}
{{--                        {{ $errors->first('user') }}--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--                <span class="help-block">{{ trans('cruds.article.fields.user_helper') }}</span>--}}
{{--            </div>--}}
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <div class="form-group">
                <label class="required" for="article_date">{{ trans('cruds.article.fields.article_date') }}</label>
                <input class="form-control date {{ $errors->has('article_date') ? 'is-invalid' : '' }}" type="text" name="article_date" id="article_date" value="{{ old('article_date') }}" required>
                @if($errors->has('article_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('article_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.article.fields.article_date_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="journal_id">{{ trans('cruds.article.fields.journal') }}</label>
                <select class="form-control select2 {{ $errors->has('journal') ? 'is-invalid' : '' }}" name="journal_id" id="journal_id">
                    @foreach($journals as $id => $entry)
                        <option value="{{ $id }}" {{ old('journal_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('journal'))
                    <div class="invalid-feedback">
                        {{ $errors->first('journal') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.article.fields.journal_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.articles.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $article->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection
